<#
This script takes a path to a CSV file as input and then uses that file to set
Home Drive and Home Path values for existing AD users.

This doesn't get used that much, but it's been handy for taking in new orgs and
when consolidating user file storage.

This script uses only the following columns/properties from the CSV file:
    - Username
    - Description

TODO: Set permissions during loop! Don't recall why this isn't already happening!
      Don't forget to uncomment the section where we set up objects related to
      permissions!

TODO: Consider moving the operations from this and from the corresponding part
      of Import-ADUsers.ps1 into a separate file/function to remove duplicate code.
#>


[cmdletbinding()]
param (
    # FilePath parameter tells where .csv input file is
    [Parameter(Mandatory=$True)][string]$FilePath
)

<# /!\ Uncomment this section when you actually add this feature
################################################################################
# Set up objects for configuring file permissions

# InheritanceFlag object for working with file permissions
$inhSubFoldersFiles = [System.Security.AccessControl.InheritanceFlags]::ContainerInherit -bor [System.Security.AccessControl.InheritanceFlags]::ObjectInherit

# PropogationFlag object for working with file permissions
$proThisFolder = [System.Security.AccessControl.PropagationFlags]::None

/!\ #>


################################################################################
# Error Checking

# check to be sure file exists; exit if not
if (-not $(Test-Path $filePath)){
    throw "File `"$filePath`" does not exist"
}

# get file info
$checkFile = dir $filePath

# check to be sure extension is .csv; exit if not
if ($checkFile.Extension -ne ".csv") {
    throw "`"$filePath`" has wrong extension: `"$($checkFile.Extension)`""
}


################################################################################
# Read file & get credentials to talk to AD

# Give some helpful output, maybe make this part of a verbose option?
Write-Verbose "Reading users from `"$filePath`""

# Make sure that file can be opened. If it doesn't, terminate w/ error
Try {
    $userImport = Import-Csv $filePath
}
Catch {
    throw "File `"$filePath`" is not readable."
}

# Add parameters for settings that cannot be set en masse

# Get credentials to run AD commands and save them
#   This isn't strictly needed if user running PowerShell session has permissions
#   to edit AD. However, it allows it to be run from accounts that don't have
#   these permissions.
$AdminCredential = Get-Credential


################################################################################
# Set Home Drive and Home Path for each listed user
#   TODO: configure permissions!

foreach ($user in $userImport)
{
    # Verbose output about account data
    Write-Verbose ""
    Write-Verbose "####################################################################"
    Write-Verbose ""
    Write-Verbose "Creating account: $($user.Username)"
    Write-Verbose "Display Name: $($user.Display)"
    Write-Verbose "Password: $($user.Password)"
    Write-Verbose "OU: $($user.OU)"
    Write-Verbose "UPN: $($user.UPN)"
    Write-Verbose ""


    # If neither HomeDrive nor HomePath is null/empty/whitespace, create user home
    if ( !([string]::IsNullorWhiteSpace($user.HomeDrive)) -and !([string]::IsNullorWhiteSpace($user.HomePath)) )
    {
        Write-Verbose "Trying to set home directory for $($user.Username)"

        # Set HomeDir variable for clearer code
        $homeDir = "$($user.HomePath)\$($user.Username)"

        # Check if directory already exists
        if ( Test-Path -Path "$homeDir" )
        {
            Set-ADUser -Identity "$($user.Username)" -HomeDirectory "$homeDir" -HomeDrive "$($user.HomeDrive):"
            Write-Verbose "$($user.Username) has home set to $($user.HomeDrive): ($homeDir)."
        }
        else
        {
            Write-Host -ForegroundColor Red "Error on $($user.Username)!!!!"
        }
    }

    # TODO: SET PERMISSIONS, TOO!
}

# END MAIN PROGRAM #############################################################
################################################################################
