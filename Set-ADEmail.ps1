<#
This script takes a path to a CSV file as input and then uses that file to add
email addresses to existing AD users.

If a user already has that property set, this script WILL NOT OVERWRITE IT

It was mostly a single-time thing when we transitioned to everyone having G Suite
accounts, but it may still be handy occasionally.

This script uses only the following columns/properties from the CSV file:
    - Username
    - Email

TODO: Consider moving the email setting operation from this and from Import-ADUsers.ps1
      into a separate file/function to remove duplicate code.

TODO: Consider adding switch parameter `-Force` to overwrite exisitng emails.

#>

[cmdletbinding()]
param (
    # FilePath parameter tells where .csv input file is
    [Parameter(Mandatory=$True)][string]$FilePath
)

################################################################################
# Error Checking

# check to be sure file exists; exit if not
if (-not $(Test-Path $filePath)){
    throw "File `"$filePath`" does not exist"
}

# get file info
$checkFile = dir $filePath

# check to be sure extension is .csv; exit if not
if ($checkFile.Extension -ne ".csv") {
    throw "`"$filePath`" has wrong extension: `"$($checkFile.Extension)`""
}

################################################################################
# Read file and get credentials to talk to AD

# Give some helpful output, maybe make this part of a verbose option?
Write-Verbose "Reading users from `"$filePath`""

# Make sure that file can be opened. If it doesn't, terminate w/ error
Try {
    $userImport = Import-Csv $filePath
}
Catch {
    throw "File `"$filePath`" is not readable."
}

# Get credentials to run AD commands and save them
#   This isn't strictly needed if user running PowerShell session has permissions
#   to edit AD. However, it allows it to be run from accounts that don't have
#   these permissions.
$AdminCredential = Get-Credential

################################################################################
# Set email for each user
foreach ($user in $userImport)
{
    # Verbose output about account data
    Write-Verbose ""
    Write-Verbose "####################################################################"
    Write-Verbose ""
    Write-Verbose "Account: $($user.Username)"
    Write-Verbose "Email: $($user.Email)"
    Write-Verbose ""

    # Get user's info, specifically the AD mail property
    $userInfo = Get-ADUser -Identity "$($user.Username)" -Properties mail

    # Make sure the mail property is currently blank; don't want to overwrite anything
    if ( [string]::IsNullorWhiteSpace($userInfo.mail) )
    {   # If it is, go ahead and set the mail value
        Set-ADUser -Identity "$($user.Username)" -EmailAddress "$($user.Email)"
        Write-Verbose "$($user.Username) has email set to `'$($user.Email)`'."
    } else { # Let us know that we're skipping in verbose mode
    Write-Verbose "MAIL EXISTS - SKIPPING ACCOUNT!!"
    }
}
