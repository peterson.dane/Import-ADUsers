<#
This script will take a path to a CSV file as input, and it will create an AD
accounts for each row. See README.md for more information about this file.

Most of the lifting for creating usernames and setting paths and such is done in
the spreadsheet before exporting a CSV file. That, too, is fairly easily to
pretty much automate with functions, but it's easier to reivew and take care of
edge cases in the spreadsheet and to let the script remain simpler.


/!\ This script assumes you've already checked for duplicate usernames.


This was one of the very first PowerShell scripts I wrote, because we desperately
needed a better account creation system than "someone just sits down and manually
creates all of them by copy-pasting". There have some some minor tweaks and
improvements, but the bulk of the script is still pretty much unchanged.

TODO: Check for existing accounts before taking any action on a given user. If
      an account exists already, give an error and continue.

TODO: Consider moving some of the password operations from this script and
      Set-ADPassword.ps1 into a separate script/function to remove duplicate
      code and incorporate more features into this script, particularly around
      $false values for `$user.PasswordReset`.

TODO: Consider moving the description setting operation from this and from
      Set-ADDescription.ps1 into a separate file/function to remove duplicate code.

TODO: Consider moving the email setting operation from this and from Set-ADEmail.ps1
      into a separate file/function to remove duplicate code.

TODO: Consider moving the HomeDir/HomePath operations from this and from
      Set-ADHomeDrive.ps1 into a separate file/function to remove duplicate code.

TODO: Consider adding additional optional(?) parameters for settings/options
      that can be set *en masse* to allow simpler CSV files.

      Things to consider:
        - Allow CSV file to override parameters if corresponding column is not blank?

#>

[cmdletbinding()]
param (
    # FilePath parameter tells where .csv input file is
    [Parameter(Mandatory=$True)][string]$FilePath,


    # Parameter to handle working with users who will primarily or initially use
    #   macOS clients. Will set up home folders for user, becaues macOS does
    #   funny things with permissions if you let it set them up.
    #   (At least, it did when I wrote this.)
    [switch]$MacOSHome
)

################################################################################
# Set up constants and arrays used in script

# InheritanceFlag object for working with file permissions
$inhSubFoldersFiles = [System.Security.AccessControl.InheritanceFlags]::ContainerInherit -bor [System.Security.AccessControl.InheritanceFlags]::ObjectInherit

# PropogationFlag object for working with file permissions
$proThisFolder = [System.Security.AccessControl.PropagationFlags]::None

# Dummy password for initial account creation (this will help with users with
#   custom password policies whose actual passwords don't meet normal complexity
#   requirements)
$dummyPassword = "l72XzsrPzGPY0iq5sVK6"

# List of directories macOS wants in home directory
#   May also want to include "Public", "Sites" and "Public/Drop Box", but
#   permissions are funny on those, and they're not really useful in multiuser
#   environments
$MacOSDirList = "Desktop","Documents","Downloads","Movies","Pictures"


################################################################################
# Error Checking

# check to be sure file exists; exit if not
if (-not $(Test-Path $filePath)){
    throw "File `"$filePath`" does not exist"
}

# get file info
$checkFile = dir $filePath

# check to be sure extension is .csv; exit if not
if ($checkFile.Extension -ne ".csv") {
    throw "`"$filePath`" has wrong extension: `"$($checkFile.Extension)`""
}

################################################################################
# Read file & get credentials to talk to AD

# Give some helpful output, maybe make this part of a verbose option?
Write-Verbose "Reading users from `"$filePath`""

# Make sure that file can be opened. If it doesn't, terminate w/ error
Try {
    $userImport = Import-Csv $filePath
}
Catch {
    throw "File `"$filePath`" is not readable."
}

# Get credentials to run AD commands and save them
#   This isn't strictly needed if user running PowerShell session has permissions
#   to edit AD. However, it allows it to be run from accounts that don't have
#   these permissions.
#
# TODO: add a check here to see if the account we're running under already has
#       adequate permissions. Consider adding a -Credential parameter, too.
$AdminCredential = Get-Credential


################################################################################
# Create and configure accounts and storage
foreach ($user in $userImport)
{
    # Verbose output about account data
    Write-Verbose ""
    Write-Verbose "####################################################################"
    Write-Verbose ""
    Write-Verbose "Creating account: $($user.Username)"
    Write-Verbose "Display Name: $($user.Display)"
    Write-Verbose "Password: $($user.Password)"
    Write-Verbose "OU: $($user.OU)"
    Write-Verbose "UPN: $($user.UPN)"
    Write-Verbose ""

    # Convert password value from plain text to secure string
    $passwordSecure = ConvertTo-SecureString -String $dummyPassword -AsPlainText -Force

    # Splat parameters for the New-ADUser command for the sake of clarity
    $NewAccountSettings = @{
        Name = "$($user.Display)"
        DisplayName = "$($user.Display)"
        GivenName = "$($user.First)"
        SamAccountName="$($user.Username)"
        Surname = "$($user.Last)"
        AccountPassword = $passwordSecure
        Path = "$($user.OU)"
        Enabled = $True
        Credential = $AdminCredential
        UserPrincipalName = "$($user.UPN)"
    }

    # Create new user account
    New-ADUser @NewAccountSettings
    Write-Verbose "Account $($user.Username) created."

    ####################################################################
    # GROUP OPERATIONS

    # If user.Group is not empty, add user to group
    if ( !([string]::IsNullorWhiteSpace($user.Group)) )
    {
        # Check if group exists. If not, create it.
        Try {
            Get-ADGroup -Identity "$($user.Group)"
        }
        Catch {
            New-ADGroup -Name "$($user.Group)" -SamAccountName "$($user.Group)"
            Write-Host -ForegroundColor Red "Group: $($user.Group) did not exist. Created in default location."
        }

        # Now that we know group exists, add user to group
        Add-ADGroupMember -Identity "$($user.Group)" -Members "$($user.Username)"
        Write-Verbose "Added $($user.Username) to $($user.Group)"
    }

    ####################################################################
    # PASSSWORD OPERATIONS
    #   doing this after group operations in case custom password policies are
    #   assigned by the group

    # Convert password value from plain text to secure string
    $passwordSecure = ConvertTo-SecureString -String $($user.Password) -AsPlainText -Force

    # Set password
    Set-ADAccountPassword -Identity "$($user.Username)" -Reset -NewPassword $passwordSecure -Credential $AdminCredential
    Write-Verbose "$($user.Username) password set to `"$($user.Password)`""

    ####################################################################
    # HOME FOLDER OPERATIONS

    # If neither HomeDrive nor HomePath is null/empty/whitespace, create user home
    if ( !([string]::IsNullorWhiteSpace($user.HomeDrive)) -and !([string]::IsNullorWhiteSpace($user.HomePath)) )
    {
        Write-Verbose "Trying to create home directory for $($user.Username)"

        # Set HomeDir variable for clearer code
        $homeDir = "$($user.HomePath)\$($user.Username)"

        # Check if directory already exists
        if ( Test-Path -Path "$homeDir" )
        {
            # If it exists, give output and don't do anything; don't want to clobber anything
            Write-Host -ForegroundColor Red "$homeDir exists. Permissions and Ownership have not changed."
            Write-Host -ForegroundColor Red "User has NOT been assigned a home dir or drive!"
        }
        else
        {
            # Otherwise, it doesn't exist, so create it
            New-Item -Path "$homeDir" -ItemType "directory"
            Write-Verbose "Created directory: $homeDir"

            ###################################################################
            # Set permissions and ownership on new directory so this user has full control

            # Create access rule for this user with Full Controll and Inheritance to subfolders and files
            $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("$($user.Username)","FullControl",$inhSubFoldersFiles,$proThisFolder,"Allow")

            # Create empty ACL object, then add the access rule to it
            $accessControl = New-Object System.Security.AccessControl.DirectorySecurity
            $accessControl.AddAccessRule($accessRule)

            #Set ownership in ACL object
            $accessControl.SetOwner([System.Security.Principal.NTAccount]"$($user.Username)")

            # Apply permissions to folder
            Set-Acl "$homeDir" $accessControl
            Write-Verbose "$($user.Username) is now owner of $homeDir"
            Write-Verbose "$($user.Username) has Full Control of $homeDir and all subfolders and files."

            Set-ADUser -Identity "$($user.Username)" -HomeDirectory "$homeDir" -HomeDrive "$($user.HomeDrive):"
            Write-Verbose "$($user.Username) has home set to $($user.HomeDrive): ($homeDir)."

            # If user will be primarily or initially using OS X clients, need to do some folder creation for them
            if ( $MacOSHome -or "$($user.MacOSHome)" -eq "1")
            {
                Write-Verbose "Creating directories for macOS user."

                # Create all the directories in the list defined in the Constants section
                #   and set ownership of files to this user
                foreach ( $directory in $MacOSDirList )
                {
                    # Create directory
                    New-Item -Path "$homeDir\$directory" -ItemType "directory"
                    Write-Verbose "Created directory: $homeDir\$directory"

                    # Get ACL from new directory
                    $newDirACL = Get-Acl "$homeDir\$directory"

                    # Set Owner in ACL to be this user
                    $newDirACL.SetOwner([System.Security.Principal.NTAccount]"$($user.Username)")

                    # Set ACL of file to match modified ACL object
                    Set-Acl "$homeDir\$directory" $newDirACL
                }
            }
        }
    }

    ####################################################################
    # ADD DESCRIPTION

    # If Description field is populated (not null/empty/whitespace), add it to user's AD account
    if ( !([string]::IsNullorWhiteSpace($user.Description)) )
    {
        Set-ADUser -Identity "$($user.Username)" -Description "$($user.Description)"
        Write-Verbose "$($user.Username) description set to `"$($user.Description)`""
    }

    ####################################################################
    # ADD EMAIL ADDRESS

    # If email address field is populated, add it to user's AD account
    if ( !([string]::IsNullorWhiteSpace($user.Email)) )
    {
        Set-ADUser -Identity "$($user.Username)" -EmailAddress "$($user.Email)"
        Write-Verbose "$($user.Username) email set to `"$($user.Email)`""
    }

    Write-Host -ForegroundColor Red "Don't forget to set up a helpdesk account for $($user.username), if they need one!"
    Write-Host ""
    Write-Host -ForegroundColor DarkCyan "####################################################################"
    Write-Host ""
}
