<#
This script will generate memorable, but still relatively strong word-based
passwords to use as temporary passwords for users. Because it's likely working
off a limited list, these should never be used as permanent passwords on
accounts with access to privileged information.

It will also generate random alphanumeric passwords of any length.

Supplying a value of 0 for the `-Random` parameter will generate memorable
word-based passwords.

After the script runs, if you supply the `-CopyPassword` parameter, it will copy
the newly-generated password into the system clipboard.

    /!\ This parameter only works on Windows, as Set-Clipboard is not part of
        PowerShell Core!

TODO: Add -Dictionary parameter and automatically capitalize one of the two
      words in memorable passwords.
#>

[cmdletbinding()]
param (
    # Path to save to file, if desired
    [string]$FilePath,

    # Number of passwords to generate; defaults to 1
    [int]$Count = 1,

    # Generate random password of specified length intead of a memorable password
    #  Defaults to 0, which will cause the script to just create a memorable one
    #  script will also generate memorable password if $Random is negative
    [int]$Random = 0,

    # Copy password directly to the system clipboard
    [switch]$CopyPassword
)


# get lowercase and uppercase words if we're making a memorable password
if ( $Random -le 0 )
{
    $words = Get-Content "$PSScriptRoot/wordlist.txt"

    $wordsUp = Get-Content "$PSScriptRoot/wordlist_upper.txt"

    #
    # get array lengths
    $lengthWords = $words.Length - 1

    $lengthWordsUp = $wordsUp.Length - 1
}

# generate passwords equal in number to Count parameter
for ( $i=1 ; $i -le $Count ; $i++)
{
    # If $Random was set ≤ 0, then make a memorable password
    if ( $Random -le 0 )
    {
        # generate random 1 digit number
        $numeral = Get-Random -Minimum 0 -Maximum 9

        # select random words from word arrays
        $word1 = Get-Random -InputObject $words

        $word2 = Get-Random -InputObject $wordsUp

        # generate password
        $password = "$word1$numeral$word2"
    } else { # Otherwise, make a random password
        # int values for [0..9][a..z][A..Z]
        $characters = 48..57 + 65..90 + 97..122

        # Convert int array into array of actual characters, use as input to
        #   create password, then join that object up into one string, intead of
        #   an array of characters
        $password = (Get-Random -Count 20 -InputObject ([char[]]$characters)) -join ""
    }

    # if FilePath is defined, write passwords to file
    if ( !([string]::IsNullorWhiteSpace($FilePath)) )
    {
        $password | Out-File "$FilePath" -Append
        Write-Verbose "$password"
    }
    else # write to stdout
    {
        Write-Output "$password"
    }
}

if ( $CopyPassword ) { Set-Clipboard -Value "$password"}
