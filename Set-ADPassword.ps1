<#
This script can be used for any bulk password resets, not just new account
creation. The only fields needed in the CSV for this script are:

    - Username
    - Password
    - PasswordReset (0/1)

TODO: Consider moving some of the password operations from this script and
      Import-ADUsers.ps1 into a separate script/function to remove duplicate
      code.
#>

[cmdletbinding()]
param (
    # FilePath parameter tells where .csv input file is
    [Parameter(Mandatory=$True)][string]$FilePath

    #TODO: Add parameters for using this as password reset/change/set utility
)

################################################################################
# Error Checking

# check to be sure file exists; exit if not
if (-not $(Test-Path $filePath)){
    throw "File `"$filePath`" does not exist"
}

# get file info
$checkFile = dir $filePath

# check to be sure extension is .csv; exit if not
if ($checkFile.Extension -ne ".csv") {
    throw "`"$filePath`" has wrong extension: `"$($checkFile.Extension)`""
}
################################################################################


################################################################################
# Read file & get credentials to talk to AD

# Give some helpful output, maybe make this part of a verbose option?
Write-Verbose "Reading users from `"$filePath`""

# Make sure that file can be opened. If it doesn't, terminate w/ error
Try {
    $userImport = Import-Csv $filePath
}
Catch {
    throw "File `"$filePath`" is not readable."
}

# Add parameters for settings that cannot be set en masse

# Get credentials to run AD commands and save them
#   This isn't strictly needed if user running PowerShell session has permissions
#   to edit AD. However, it allows it to be run from accounts that don't have
#   these permissions.
$AdminCredential = Get-Credential


################################################################################
# Set password and mark accounts for password reset or set password to never expire
foreach ($user in $userImport)
{
    # Verbose output about account data
    Write-Verbose "Resetting password for $($user.Username) to $($user.Password)"

    # Convert password value from plain text to secure string
    $passwordSecure = ConvertTo-SecureString -String $user.Password -AsPlainText -Force

    # Set password
    Set-ADAccountPassword -Identity "$($user.Username)" -Reset -NewPassword $passwordSecure -Credential $AdminCredential

    # Set account to ask for password reset if flag is set
    if ( "$($user.PasswordReset)" -eq "1" )
    {
        Set-ADUser -Identity "$($user.Username)" -PasswordNeverExpires $false
        Set-ADUser -Identity "$($user.Username)" -ChangePasswordAtLogon $true
    }
    else
    {
        Set-ADUser -Identity "$($user.Username)" -PasswordNeverExpires $true
    }
}
