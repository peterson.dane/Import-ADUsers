<#
This script takes a path to a CSV file as input and then uses that file to add
descriptions to existing AD users.

This script uses only the following columns/properties from the CSV file:
    - Username
    - Description

TODO: Consider moving the description setting from this and from Import-ADUsers.ps1
      into a separate file/function to remove duplicate code.
#>


[cmdletbinding()]
param (
    # FilePath parameter tells where .csv input file is
    [Parameter(Mandatory=$True)][string]$FilePath
)

################################################################################
# Error Checking

# check to be sure file exists; exit if not
if (-not $(Test-Path $filePath)){
    throw "File `"$filePath`" does not exist"
}

# get file info
$checkFile = dir $filePath

# check to be sure extension is .csv; exit if not
if ($checkFile.Extension -ne ".csv") {
    throw "`"$filePath`" has wrong extension: `"$($checkFile.Extension)`""
}

################################################################################
# Read file & get credentials to talk to AD

# Give some helpful output, maybe make this part of a verbose option?
Write-Verbose "Reading users from `"$filePath`""

# Make sure that file can be opened. If it doesn't, terminate w/ error
Try {
    $userImport = Import-Csv $filePath
}
Catch {
    throw "File `"$filePath`" is not readable."
}

# Add parameters for settings that cannot be set en masse

# Get credentials to run AD commands and save them
#   This isn't strictly needed if user running PowerShell session has permissions
#   to edit AD. However, it allows it to be run from accounts that don't have
#   these permissions.
$AdminCredential = Get-Credential

################################################################################
# Set Descriptions
foreach ($user in $userImport)
{
    # Verbose output about account data
    Write-Verbose ""
    Write-Verbose "####################################################################"
    Write-Verbose ""
    Write-Verbose "Account: $($user.Username)"
    Write-Verbose "Display Name: $($user.Display)"
    Write-Verbose ""


    Set-ADUser -Identity "$($user.Username)" -Description "$($user.Description):"
    Write-Verbose "$($user.Username) has description set to `'$($user.Description)`'."

}
