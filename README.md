# Import-ADUsers

These scripts allow for easy setup of accounts *en masse*, based on a CSV file.

See comments in individual scripts for more information about each script's behavior.

## CSV File Setup

The CSV file used to import accounts must have the following columns:

- **Last** - Surname of user; will set the AD attribute `sn`.

- **First** - Given name of user; will set the AD attribute `givenName`.

- **Display** - Will be used to set the AD attribute `displayName`.

- **Username** - Will be used to set the AD attribute `sAMAccountName`.

- **Email** - Email address of user; will be used to set the AD attribute `mail`.

- **UPN** - Will be used the set the AD attribute `userPrincipalName`.

- **Password** - Will be used to set the user's password.

- **OU** - The OU under which to create the user account. This should be the distinguished name (DN) for the OU, in the format: `OU=grandchild,OU=child,OU=parent,DC=subdomain,DC=domain,DC=tld`

- **Description** - Will beused to set the AD attribute `description`.

- **Group** - Will be used to add the user to one group. `Import-ADUsers.ps1` will automatically create this group in the default location if it does not already exist. Currently, this script can take in only one group for each user.

- **HomeDrive** - Map the user's network home directory to this drive. Used to set the AD attribute `homeDrive`.

- **HomePath** - UNC path in which to create the user's home directory. This ***should not*** include their named home directory; the script will take care of creating that based on the `Username` column. This value will be combined with `Username` to set the AD attribute `homeDirectory`.

- **PasswordReset** - Instructs the script as to whether or not to set the user's account for a password reset on their next login. Valid values are `0` or `1`.

- **MacOSHome** - Instructs the script as to whether or not to pre-create some macOS-style subdirectories in their new home directory. Valid values are `0` or `1`.
